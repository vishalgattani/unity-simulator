// #if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;
using UnityEngine.SceneManagement;
using SFB;

public class LoadAssetFromAssetBundle : MonoBehaviour
{
    public List<GameObject> targetList;
    public List<Texture2D> iconsList;
    public Transform AssetContent;
    public GameObject AssetItem;
    public AssetBundle bundle;

    // These are only used for generating the icon & debugging
    [Header("Debugging")]
    public Texture2D icon;
    string homeDirectory;

    void Start ()
	{
        homeDirectory = GetHomeDirectory();
        Debug.Log("Home Directory: " + homeDirectory);
    }

    string GetHomeDirectory()
    {
        string homeDirectory;

        // Check the current platform
        switch (Application.platform)
        {
            case RuntimePlatform.WindowsPlayer:
            case RuntimePlatform.WindowsEditor:
                homeDirectory = System.Environment.GetEnvironmentVariable("USERPROFILE");
                break;

            case RuntimePlatform.OSXPlayer:
            case RuntimePlatform.OSXEditor:
                homeDirectory = System.Environment.GetEnvironmentVariable("HOME");
                break;

            case RuntimePlatform.LinuxPlayer:
            case RuntimePlatform.LinuxEditor:
                homeDirectory = System.Environment.GetEnvironmentVariable("HOME");
                break;

            default:
                homeDirectory = "";
                break;
        }

        return homeDirectory;
    }

    public void OpenExplorer(){
        if (GameObject.Find("AssetSpawner") == null){
            GameObject EmptyObj = new GameObject("AssetSpawner");
        }
        int targetCount = 0;
		string iconName;

        // get path of bundle to be loaded
        string[] path = StandaloneFileBrowser.OpenFilePanel("Load asset bundle for asset inventory",homeDirectory,"",false);
        
        if (path[0] != ""){
            // load asset bundle from path
            bundle = AssetBundle.LoadFromFile(path[0]);
            string assetBundleDirectory = Path.Combine(Application.dataPath,"AssetBundles",bundle.name);
            targetList = new List<GameObject>(bundle.LoadAllAssets<GameObject>());
            iconsList = new List<Texture2D>(bundle.LoadAllAssets<Texture2D>());
            // Do something with the loaded textures (e.g., apply to materials, use in the project)
            foreach (Texture2D texture in iconsList)
            {
                // Apply each texture to a material or use it in your project
                Renderer renderer = GetComponent<Renderer>();
                if (renderer != null)
                {
                    renderer.material.mainTexture = texture;
                }
            }

            foreach (Transform item in AssetContent){
                Destroy(item.gameObject);
            }

            for (int i=0; i<targetList.Count;i++)
            {
                GameObject targetObj = targetList[i];
                Debug.Log(targetObj.name);
                icon = iconsList[i] as Texture2D;
                iconName = targetObj.name;
                GameObject temp = Instantiate(AssetItem,AssetContent);
                temp.name = targetObj.name;
                temp.GetComponent<Button>().image.sprite = Sprite.Create(icon, new Rect(0, 0, icon.width, icon.height), new Vector2(0.5f, 0.5f));
                temp.GetComponent<AssetItem>().assetName = targetObj.name;
                temp.GetComponent<AssetItem>().assetBundleName = bundle.name;
                temp.GetComponent<AssetItem>().asset = targetObj;
                targetCount++;
            }
        }
        
        
    }

    public void ClearLoadedAssetBundle(){
        if(bundle!=null){
            bundle.Unload(false);
            foreach (Transform item in AssetContent){
                Destroy(item.gameObject);
            }
        }
    }

    public void ClearSpawnedObjects(){
        ClearLoadedAssetBundle();
        GameObject temp = GameObject.Find("AssetSpawner");
        if (temp!=null){
            Destroy(temp);
        }
    }

    public void SaveObjectConfigurationButton(){
        GameObject temp = GameObject.Find("AssetSpawner");
        if (temp!=null){
            SaveByJSON();
        }
        else{
            Debug.Log("No spawned objects using this tool to save!");
        }
    }

    private void SaveByJSON(){

        List<SaveObjectConfiguration> saves = createSaveObjectConfiguration();
        string JsonString = "["; 
        foreach(SaveObjectConfiguration save in saves){
            JsonString+=JsonUtility.ToJson(save)+",\n"; //Convert SAVE Object into JSON(String)
        } 
        JsonString = JsonString.Substring(0, JsonString.Length - 2);
        JsonString+="]";
        string path = StandaloneFileBrowser.SaveFilePanel("SaveObjectConfiguration Assets",homeDirectory,"obstacle_configuration.json",".json");
        StreamWriter sw = new StreamWriter(path);
        sw.Write(JsonString);
        sw.Close();
        Debug.Log("-=-=-=-SAVED-=-=-=-");
        
    }

    public void SaveWaypointConfigurationButton(){
        GameObject temp = GameObject.Find("AssetSpawner");
        if (temp!=null){
            SaveByYAML();
        }
        else{
            Debug.Log("No spawned objects using this tool to save!");
        }
    }

    private void SaveByYAML(){
        List<SaveWaypointConfiguration> saves = createSaveWaypointConfiguration();
        // Create YAML string manually for multiple waypoints
        string yamlString = "version: 1.0\nwaypoints:\n";

        foreach (var save in saves)
        {
            yamlString += "  - radius: " + save.waypoint.radius + "\n";
            yamlString += "    pose:\n";
            yamlString += "      yaw: " + save.waypoint.pose.yaw + "\n";
            yamlString += "      x: " + save.waypoint.pose.x + "\n";
            yamlString += "      y: " + save.waypoint.pose.y + "\n";
            yamlString += "      z: " + save.waypoint.pose.z + "\n";
            yamlString += "    frame_id: " + save.waypoint.frame_id + "\n";
        }
        // Save to file
        string path = StandaloneFileBrowser.SaveFilePanel("SaveWaypointConfiguration",homeDirectory,"waypoints.txt","txt");
        if (!string.IsNullOrEmpty(path)){
            Debug.Log(yamlString);
            StreamWriter sw = new StreamWriter(path);
            sw.Write(yamlString);
            sw.Close();
            Debug.Log("YAML file saved: " + path);
            Debug.Log("-=-=-=-SAVED-=-=-=-");
        }
    }

    private List<SaveObjectConfiguration> createSaveObjectConfiguration(){
        List<SaveObjectConfiguration> saves = new List<SaveObjectConfiguration>();
        
        GameObject temp = GameObject.Find("AssetSpawner");
        if (temp.transform.childCount > 0){

            foreach (Transform child in temp.transform){
                SaveObjectConfiguration save = new SaveObjectConfiguration();
                Debug.Log(child.name);
                save.name = child.name.Replace("(Clone)","").Trim();
                save.info.name = save.name;
                save.info.full_name = "";
                save.info.asset_bundle = child.GetComponent<AssetItem>().assetBundleName;

                Vector3 pos = child.transform.position;
                Vector3 normalized_pos = Vector3.Normalize(pos);
                save.position.x = pos.x;
                save.position.y = pos.y;
                save.position.z = pos.z;
                save.position.magnitude = pos.magnitude;
                save.position.sqrMagnitude = pos.sqrMagnitude;
                save.position.normalized.x = normalized_pos.x;
                save.position.normalized.y = normalized_pos.y;
                save.position.normalized.z = normalized_pos.z;
                save.position.normalized.magnitude = normalized_pos.magnitude;
                save.position.normalized.sqrMagnitude = normalized_pos.sqrMagnitude;
                
                Quaternion rot = child.transform.rotation;
                Vector3 eulerRot = rot.eulerAngles;
                Vector3 normalized_rot = Vector3.Normalize(eulerRot);
                save.rotation.x = rot.x;
                save.rotation.y = rot.y;
                save.rotation.z = rot.z;
                save.rotation.w = rot.w;
                save.rotation.euler_angles.x = eulerRot.x;
                save.rotation.euler_angles.y = eulerRot.y;
                save.rotation.euler_angles.z = eulerRot.z;
                
                save.rotation.euler_angles.magnitude = eulerRot.magnitude;
                save.rotation.euler_angles.sqrMagnitude = eulerRot.sqrMagnitude;
                save.rotation.euler_angles.normalized.x = normalized_rot.x;
                save.rotation.euler_angles.normalized.y = normalized_rot.y;
                save.rotation.euler_angles.normalized.z = normalized_rot.z;
                save.rotation.euler_angles.normalized.magnitude = normalized_rot.magnitude;
                save.rotation.euler_angles.normalized.sqrMagnitude = normalized_rot.sqrMagnitude;

                Vector3 scl = child.transform.localScale;
                Vector3 normalized_scl = Vector3.Normalize(scl);
                save.scale.x = scl.x;
                save.scale.y = scl.y;
                save.scale.z = scl.z;
                save.scale.magnitude = scl.magnitude;
                save.scale.sqrMagnitude = scl.sqrMagnitude;
                save.scale.normalized.x = normalized_scl.x;
                save.scale.normalized.y = normalized_scl.y;
                save.scale.normalized.z = normalized_scl.z;
                save.scale.normalized.magnitude = normalized_scl.magnitude;
                save.scale.normalized.sqrMagnitude = normalized_scl.sqrMagnitude;

                saves.Add(save);
            }
        }
        return saves;
    }

    private List<SaveWaypointConfiguration> createSaveWaypointConfiguration(){
        List<SaveWaypointConfiguration> saves = new List<SaveWaypointConfiguration>();
        
        GameObject temp = GameObject.Find("AssetSpawner");
        if (temp.transform.childCount > 0){

            foreach (Transform child in temp.transform){
                SaveWaypointConfiguration save = new SaveWaypointConfiguration();
                Debug.Log(child.name);
                // save.name = child.name.Replace("(Clone)","").Trim();

                Vector3 pos = child.transform.position;
                Vector3 normalized_pos = Vector3.Normalize(pos);
                save.waypoint.pose = new Pose();
                save.waypoint.pose.x = pos.x;
                save.waypoint.pose.y = pos.y;
                save.waypoint.pose.z = pos.z;
                save.waypoint.frame_id = "world";

                save.waypoint.radius = 1.0f;
                
                Quaternion rot = child.transform.rotation;
                Vector3 eulerRot = rot.eulerAngles;
                save.waypoint.pose.yaw = eulerRot.z;
                saves.Add(save);
            }
        }
        return saves;
    }    

}

    [System.Serializable]
    public class SaveWaypointConfiguration
    {
        public Waypoint waypoint = new Waypoint();
    }

    [System.Serializable]
    public class Waypoint
    {
        public float radius;
        public Pose pose = new Pose();
        public string frame_id;
    }

    [System.Serializable]
    public class Pose
    {
        public float yaw;
        public float x;
        public float y;
        public float z;
    }

[System.Serializable]
public class Info
{
    public string name;
    public string full_name;
    public string asset_bundle;

}

[System.Serializable]
public class SaveObjectConfiguration
{
    public string name;
    public Info info = new Info();
    public PositionJSON position = new PositionJSON();
    public RotationJSON rotation = new RotationJSON();
    public ScaleJSON scale = new ScaleJSON();
}

[System.Serializable]
public class PositionJSON
{
    public float x;
    public float y;
    public float z;
    public float magnitude;
    public float sqrMagnitude;
    public NormalizedDictJSON normalized = new NormalizedDictJSON();
}

[System.Serializable]
public class RotationJSON
{
    public float x;
    public float y;
    public float z;
    public float w;
    // Quaternion rotation = Quaternion.Euler(0, 30, 0);
    public EulerAnglesJSON euler_angles = new EulerAnglesJSON();
}

[System.Serializable]
public class EulerAnglesJSON
{
    public float x;
    public float y;
    public float z;
    public float magnitude;
    public float sqrMagnitude;
    public NormalizedDictJSON normalized = new NormalizedDictJSON();
}

[System.Serializable]
public class ScaleJSON
{
    public float x;
    public float y;
    public float z;
    public float magnitude;
    public float sqrMagnitude;
    public NormalizedDictJSON normalized = new NormalizedDictJSON();
}

[System.Serializable]
public class NormalizedDictJSON
{
    public float x;
    public float y;
    public float z;
    public float magnitude;
    public float sqrMagnitude;
}

// #endif
