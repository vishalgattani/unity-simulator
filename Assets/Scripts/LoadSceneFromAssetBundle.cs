#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;

public class LoadSceneFromAssetBundle : MonoBehaviour
{
    public string path;
    public async void OpenExplorer(){

        // get path of bundle to be loaded
        path = EditorUtility.OpenFilePanel("Load Bundle","","");
        string currentDirectory = Path.GetDirectoryName(path);
        // Debug.Log(currentDirectory);

        // load asset bundle from path
        AssetBundle bundle;
        bundle = AssetBundle.LoadFromFile(path);
        // Debug.Log(bundle);

        // get scene from assetbundle
        string[] scenePath = bundle.GetAllScenePaths();
        var flag = 0;
        var sceneNum = -1;
        for(int i = 0; i < scenePath.Length; i++)
        {
            if(scenePath[i].Contains(".unity")){
                flag = 1;
                sceneNum = i;
                break;
            }
        }

        if(flag == 1){
            Debug.Log("Loading " + scenePath[sceneNum]);
            // Load the scene asynchronously and await the operation to complete.
            await LoadSceneFromBundleAsync(scenePath[sceneNum]);
        }
    }

    public async Task LoadSceneFromBundleAsync(string scenePath)
    {
        // Load the scene asynchronously and await the operation to complete.
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(scenePath, LoadSceneMode.Additive);
        while (!asyncOperation.isDone)
        {
            float progress = Mathf.Clamp01(asyncOperation.progress / 0.9f);
            // You can use the progress value to update a loading bar or display loading progress.
            // Example: loadingProgressBar.fillAmount = progress;
            await Task.Yield(); // Wait for the next frame to avoid blocking the main thread.
        }
    }
}


#endif