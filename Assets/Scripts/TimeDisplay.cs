using UnityEngine;
using UnityEngine.UI;
using System;

public class TimeDisplay : MonoBehaviour
{
    public Text timeText; // Reference to the UI Text component for time.
    public Text fpsText; // Reference to the UI Text component for FPS.

    private TimeServer timeServer;

    private float deltaTime = 0.0f;

    private void Start()
    {
        timeServer = TimeServer.Instance;

        if (timeText == null)
        {
            Debug.LogError("Please assign the TimeText UI Text component in the Inspector.");
        }

        if (fpsText == null)
        {
            Debug.LogError("Please assign the FPSText UI Text component in the Inspector.");
        }
    }

    private void Update()
    {
        if (timeText != null && timeServer != null)
        {
            float currentTime = timeServer.GetTime();
            TimeSpan timeSpan = TimeSpan.FromSeconds(currentTime);
            string formattedTime = string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D3}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds, timeSpan.Milliseconds);
            timeText.text = "Time: " + formattedTime;
        }

        // Calculate frames per second.
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
        float fps = 1.0f / deltaTime;
        string formattedFPS = string.Format("FPS: {0:F2}", fps);
        fpsText.text = formattedFPS;
    }
}
