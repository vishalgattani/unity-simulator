using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform cameraTransform;
    public bool enableControl = false;
    private bool cursorLocked = false; // Track cursor lock state
    // Currently tracked transform
    public Vector3 zoomAmount = new Vector3(0,-10,10);
    public Vector3 newZoom;
    public float movementSpeed = 1;
    public float movementTime = 100;
    public Vector3 newPosition;
    public float normalSpeed = 1;
    public float fastSpeed = 3;
    public float rotationAmount = 1;
    public Quaternion newRotation;
    Vector2 rotation = new Vector2(0, 0);

    // Start is called before the first frame update
    void Start()
    {
        cameraTransform = Camera.main.transform;
        // Ensure that the cursor is initially locked
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        newPosition = transform.position;
        newRotation = transform.rotation;
        newZoom = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            // Toggle camera control and cursor lock
            enableControl = !enableControl;
            cursorLocked = enableControl;
            Cursor.lockState = cursorLocked ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !cursorLocked; // Show/hide the cursor
        }
        else if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Release camera control and unlock the cursor
            enableControl = false;
            cursorLocked = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true; // Show the cursor
        }

        if (enableControl)
        {
            HandleMovementInput();
            HandleMouseInput();
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false; // Hide the cursor
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true; // Show the cursor
        }

    }

    void HandleMouseInput(){
        // Rotate the camera based on the mouse movement
        float sensitivity = 5.0f;
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        transform.eulerAngles += new Vector3(-mouseY * sensitivity, mouseX * sensitivity, 0);

    }

    void HandleMovementInput(){
        if(Input.GetKey(KeyCode.LeftShift)){
            movementSpeed = fastSpeed;
        }
        else{
            movementSpeed = normalSpeed;
        }
        movementSpeed = (float)(movementSpeed * 0.1);
        if(Input.GetKey(KeyCode.W)||Input.GetKey(KeyCode.UpArrow)){
            newPosition += transform.forward*movementSpeed;
        }
        if(Input.GetKey(KeyCode.S)||Input.GetKey(KeyCode.DownArrow)){
            newPosition += transform.forward*-movementSpeed;
        }
        if(Input.GetKey(KeyCode.A)||Input.GetKey(KeyCode.LeftArrow)){
            newPosition += transform.right*-movementSpeed;
        }
        if(Input.GetKey(KeyCode.D)||Input.GetKey(KeyCode.RightArrow)){
            newPosition += transform.right*movementSpeed;
        }

        if(Input.GetKey(KeyCode.Q)){
            newRotation *= Quaternion.Euler(Vector3.up*rotationAmount);
        }
        if(Input.GetKey(KeyCode.E)){
            newRotation *= Quaternion.Euler(Vector3.up*-rotationAmount);
        }

        if(Input.GetKey(KeyCode.R)){
            newZoom += zoomAmount;
        }
        if(Input.GetKey(KeyCode.G)){
            newZoom -= zoomAmount;
        }

        transform.position = Vector3.Lerp(transform.position,newPosition,Time.deltaTime*movementTime);
        // transform.rotation = Quaternion.Lerp(transform.rotation,newRotation,Time.deltaTime*movementTime);
        // cameraTransform.localPosition = Vector3.Lerp(cameraTransform.localPosition,newZoom,Time.deltaTime*movementTime);
    }

}
