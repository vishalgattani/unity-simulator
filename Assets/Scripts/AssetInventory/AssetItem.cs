using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AssetItem : MonoBehaviour
{
    public string assetName;
    public string assetBundleName;
    public string assetPath;
    public GameObject asset;

    [SerializeField] Material assetPreviewMeshMat;
    [SerializeField] private GameObject[] placeableObjectPrefabs;
    
    private Mesh assetPreviewMesh; 
    
    private GameObject currentPlaceableObject;

    private float mouseWheelRotation = 0f;
    private int currentPrefabIndex = -1;
    private Vector3 pos;
    public float rotationSpeed = 5f;
    private float arrowRotationAmount = 0f;
            
    private Vector3 lastMousePosition;
    private bool isRotating = false;

    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        HandleKeyboardInput();
        MoveCurrentObjectToMouse();
        RotateFromMouseWheel();
        RotateFromArrows();
        ReleaseIfClicked();

    }

    public void HandleKeyboardInput(){
        if (currentPlaceableObject != null){
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Destroy(currentPlaceableObject);
            }
        }
    }

    public void HandleMouseInput(){
        
        if (assetName != null){
            if (currentPlaceableObject != null)
            {
                Destroy(currentPlaceableObject);
            }
            currentPlaceableObject = Instantiate(asset);
            GameObject assetSpawner = GameObject.Find("AssetSpawner");
            currentPlaceableObject.transform.parent = assetSpawner.gameObject.transform;
            currentPlaceableObject.GetComponent<BoxCollider>().enabled = false;
            currentPlaceableObject.GetComponent<BoxCollider>().isTrigger = false;
            DisableColliders(currentPlaceableObject);
            currentPlaceableObject.AddComponent<AssetItem>();
            currentPlaceableObject.GetComponent<AssetItem>().assetBundleName = assetBundleName;
        }
        
    }

    private void MoveCurrentObjectToMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit,50000.0f))
        {
            // if (hit.collider.CompareTag("Spawnable") || hit.collider.CompareTag("Player")){
            if (!hit.transform.name.EndsWith("(Clone)")){
                if (currentPlaceableObject!=null){  
                    Debug.Log(hit.point);
                    currentPlaceableObject.transform.position = hit.point;
                    currentPlaceableObject.transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
                    RotateFromMouseWheel();
                    RotateFromArrows();
                }
            }
        }
    }

    void DisableColliders(GameObject obj){
        // Get all colliders attached to the object
        Collider[] colliders = obj.GetComponents<Collider>();

        // Disable each collider
        foreach (Collider collider in colliders)
        {
            collider.enabled = false;
        }

        // Disable colliders on children recursively
        foreach (Transform child in obj.transform)
        {
            DisableColliders(child.gameObject);
        }
    }

    private void RotateFromMouseWheel()
    {
        if(currentPlaceableObject!=null && Input.mouseScrollDelta.y > 0.0f){
            mouseWheelRotation += Input.mouseScrollDelta.y;
            currentPlaceableObject.transform.Rotate(Vector3.up, mouseWheelRotation * 10f);
        }
        
    }

    private void RotateFromArrows()
    {
        if (currentPlaceableObject != null)
        {
            float rotationAmount = rotationSpeed; // Adjust this value as needed

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                arrowRotationAmount += rotationAmount;
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                arrowRotationAmount -= rotationAmount;
            }
            currentPlaceableObject.transform.Rotate(Vector3.up, arrowRotationAmount);
        }
    }

    private void ReleaseIfClicked()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(currentPlaceableObject!=null){
                currentPlaceableObject.GetComponent<BoxCollider>().enabled = true;
                currentPlaceableObject.GetComponent<BoxCollider>().isTrigger = false;
            }
            currentPlaceableObject = null;
        }
    }


}
