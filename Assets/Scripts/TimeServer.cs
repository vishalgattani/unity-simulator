using UnityEngine;

public class TimeServer : MonoBehaviour
{
    private static TimeServer instance;
    public static TimeServer Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<TimeServer>();
                if (instance == null)
                {
                    GameObject go = new GameObject("TimeServer");
                    instance = go.AddComponent<TimeServer>();
                }
            }
            return instance;
        }
    }

    private float currentTime = 0.0f;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        // Update the current time as needed.
        currentTime += Time.deltaTime;
    }

    public float GetTime()
    {
        return currentTime;
    }
}
