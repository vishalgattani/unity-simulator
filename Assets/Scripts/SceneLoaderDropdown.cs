using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Threading.Tasks;

public class SceneLoaderDropdown : MonoBehaviour
{
    public Dropdown dropdown;
    public Text selectedName;
    private int indexChosen;
    List<string> envs = new List<string>();


    public void DropdownIndexSelected(int index){
        selectedName.text = dropdown.options[index].text;
        Debug.Log(index);
        indexChosen = index;
    }

    public async void LoadSceneFromDropdown()
    {
        // Load the scene asynchronously and await the operation to complete.
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(indexChosen,LoadSceneMode.Additive);
        while (!asyncOperation.isDone)
        {
            float progress = Mathf.Clamp01(asyncOperation.progress / 0.9f); // 0.9 is used as the maximum progress value
            // You can use the progress value to update a loading bar or display loading progress.
            // Example: loadingProgressBar.fillAmount = progress;

            await Task.Yield(); // Wait for the next frame to avoid blocking the main thread.
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        PopulateDropdown();


    }

    void PopulateDropdown(){
        // envs.Add("Please Select Environment")
        dropdown.options.Add(new Dropdown.OptionData("Please Select..."));

        for(int i = 1; i < SceneManager.sceneCountInBuildSettings; ++i) {
            string name = System.IO.Path.GetFileNameWithoutExtension(SceneUtility.GetScenePathByBuildIndex(i));
            // envs.Add(name);
            dropdown.options.Add(new Dropdown.OptionData(name));
        }

    }


    // Update is called once per frame
    void Update()
    {

    }
}
