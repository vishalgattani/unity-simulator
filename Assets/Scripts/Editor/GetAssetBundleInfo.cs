using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using static System.Reflection.BindingFlags;

public class GetAssetBundleInfo : MonoBehaviour {

    [MenuItem ("Assets/Get Asset Bundle Info")]
    static void GetNames ()
    {

        Dictionary<string, string> dic = new Dictionary<string, string>();
        List<List<string>> listofassets = new List<List<string>>();
        string assetBundleDirectory = "Assets/AssetInfo";
        if(!Directory.Exists(assetBundleDirectory))
        {
            FileUtil.DeleteFileOrDirectory(assetBundleDirectory);
            Directory.CreateDirectory(assetBundleDirectory);
        }

        string filename = Application.dataPath+"/AssetInfo/asset_info.csv";

        var names = AssetDatabase.GetAllAssetBundleNames();

        foreach( var assetBundleName in AssetDatabase.GetAllAssetBundleNames() ){
            string assetbundlefile = Application.dataPath+"/AssetInfo/"+assetBundleName+"_asset_info.csv";
            TextWriter fp = new StreamWriter(assetbundlefile,true);
            foreach( var assetPathAndName in AssetDatabase.GetAssetPathsFromAssetBundle(assetBundleName) ){
                // var prefabPath = Path.GetFileNameWithoutExtension(assetPathAndName );
                Debug.Log( assetPathAndName );
                GameObject loadedPrefab = PrefabUtility.LoadPrefabContents(assetPathAndName);
                if(loadedPrefab.GetComponent<BoxCollider>() != null){
                    BoxCollider collider = loadedPrefab.GetComponent<BoxCollider>();
                    Debug.Log(assetPathAndName);
                    Debug.Log(collider.bounds.extents.x);
                    string nameWithoutPath = assetPathAndName.Substring( assetPathAndName.LastIndexOf( "/" ) + 1 );
                    string name = nameWithoutPath.Substring( 0, nameWithoutPath.LastIndexOf( "." ) );
                    float x = 2*collider.bounds.extents.x;
                    float y = 2*collider.bounds.extents.y;
                    float z = 2*collider.bounds.extents.z;
                    fp.WriteLine(name+","+assetPathAndName+","+assetBundleName+","+x+","+y+","+z);
                }
            }
            fp.Close();
        }
    }
}