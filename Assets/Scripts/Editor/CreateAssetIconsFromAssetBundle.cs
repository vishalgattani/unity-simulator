#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;
using UnityEngine.SceneManagement;

public class CreateAssetIconsFromAssetBundle : MonoBehaviour
{
    [MenuItem ("Assets/Create Asset Icons")]
    static void GenerateIcons()
    {
        List<GameObject> targetList;
        List<string> targetListPaths;
        List<Texture2D> iconsList;
        List<string> iconsListPaths;
        var names = AssetDatabase.GetAllAssetBundleNames();

        foreach (string name in names){
            int targetCount = 0;
            // load asset bundle from path
            // AssetBundle bundle = AssetBundle.LoadFromFile(Application.dataPath+ "/AssetBundles/" + name);
            string assetBundleDirectory = Path.Combine(Application.dataPath,"Icons",name);

            if(!Directory.Exists(assetBundleDirectory))
            {
                FileUtil.DeleteFileOrDirectory(assetBundleDirectory);
                Directory.CreateDirectory(assetBundleDirectory);
            }
            else{
                FileUtil.DeleteFileOrDirectory(assetBundleDirectory);
                Directory.CreateDirectory(assetBundleDirectory);
            }

            foreach( var assetPathAndName in AssetDatabase.GetAssetPathsFromAssetBundle(name) ){
                Debug.Log( assetPathAndName );
                if (assetPathAndName.EndsWith("unity")){
                    // If the asset path ends with "unity," break out of the loop
                    break;
                }
                GameObject loadedPrefab = PrefabUtility.LoadPrefabContents(assetPathAndName);
                Texture rawIcon = AssetPreview.GetAssetPreview (loadedPrefab);
                Texture2D icon = rawIcon as Texture2D;

                if(!Directory.Exists(assetBundleDirectory))
                {
                    FileUtil.DeleteFileOrDirectory(assetBundleDirectory);
                    Directory.CreateDirectory(assetBundleDirectory);
                    Debug.Log("Created "+assetBundleDirectory);
                }
                SaveTextureToFile(icon, assetBundleDirectory,loadedPrefab.name+".png");
                
            }

        }
    }

    static void SaveTextureToFile(Texture2D texture, string bundle_name, string fileName)
    {
        // Get the raw texture data
        byte[] bytes = texture.GetRawTextureData();

        // Create a new Texture2D and load the raw data into it
        Texture2D newTexture = new Texture2D(texture.width, texture.height, texture.format, texture.mipmapCount > 1);
        newTexture.LoadRawTextureData(bytes);
        newTexture.Apply();

        // Encode the texture to PNG
        byte[] encodedBytes = newTexture.EncodeToPNG();

        // Save to file
        File.WriteAllBytes(Path.Combine(Application.dataPath, bundle_name,fileName), encodedBytes);
        Debug.Log ("File saved in: " + Path.Combine(Application.dataPath, bundle_name,fileName) + ".png");
    }

}
#endif