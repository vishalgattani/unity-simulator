using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    // Start is called before the first frame update
    public void startSimulator()
    {
        SceneManager.LoadScene(1);
    }

    // Update is called once per frame
    public void goBack()
    {
        int build_index = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(build_index-1);
    }
}
