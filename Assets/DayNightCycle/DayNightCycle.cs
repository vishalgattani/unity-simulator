﻿﻿
using UnityEngine.UI;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;



public class DayNightCycle : MonoBehaviour {

    float deltaTime = 0.0f;
    private TimeServer timeServer;

    private float last_sys_time;
    private float last_sim_time;
    private float avg_rtf;


    [Header("Time")]
    [Tooltip("Day Length in Minutes")]
    [SerializeField]
    private float _targetDayLength = 0.5f; //length of day in minutes
    public float targetDayLength
    {
        get
        {
            return _targetDayLength;
        }
    }
    [SerializeField]
    private float elapsedTime;
    [SerializeField]
    private bool use24Clock = true;
    [SerializeField]
    [Range(0f, 24f)]
    private float _timeOfDay;
    // public float timeOfDay
    // {
    //     get
    //     {
    //         return _timeOfDay;
    //     }
    // }


    private float _timeScale = 1.0f;
    private bool _dToD = false;

    [SerializeField]
    public bool pause = false;
    [SerializeField]
    private AnimationCurve timeCurve;

    private float timeCurveNormalization;

    [Header("Sun Light")]
    [SerializeField]
    private Transform dailyRotation;
    [SerializeField]
    private Light sun;
    private float intensity;
    [SerializeField]
    private float sunBaseIntensity = 1f;
    [SerializeField]
    private float sunVariation = 1.5f;
    [SerializeField]
    private Gradient sunColor;

    private static float getSet_ToD;
    private static float getSet_TimeScale;
    private bool getSet_Dynamic_ToD;
    private GameObject getSettings;

    // [Header("Seasonal Variables")]
    // [SerializeField]
    // private Transform sunSeasonalRotation;
    // [SerializeField]
    // [Range(-45f, 45f)]
    // private float maxSeasonalTilt;

    [Header("Modules")]
    private List<DNModuleBase> moduleList = new List<DNModuleBase>();

    private void Start()
    {
        // getSettings = GameObject.Find("Settings");
        // getSet_ToD = getSettings.GetComponent<Settings>().timeOfDay;
        // getSet_TimeScale = getSettings.GetComponent<Settings>().timeScale;
        // getSet_Dynamic_ToD = getSettings.GetComponent<Settings>().DynamicTimeOfDay;

        // _timeOfDay = getSet_ToD;
        // _dToD = getSet_Dynamic_ToD;

        timeServer = GameObject.Find("TimeServer").GetComponent<TimeServer>() as TimeServer;
        if(timeServer == null) {
            Debug.LogError("Could not find TimeServer!");
        }
    }

    private void Update()
    {

        // getSet_Dynamic_ToD = getSettings.GetComponent<Settings>().DynamicTimeOfDay;
        // _dToD = getSet_Dynamic_ToD;
        // pause = !_dToD;

        // if (_dToD)
        // {
            // UpdateTimeScale();
        UpdateTime();
            // UpdateClock();
        // }

        AdjustSunRotation();
        SunIntensity();
        AdjustSunColor();
        UpdateModules(); //will update modules each frame
        EnableDisableSunMoon(false);
    }

    private void UpdateTimeScale()
    {
        _timeScale = 1; //changes timescale based on time curve
    }

    private void UpdateTime()
    {
        // getSettings = GameObject.Find("Settings");
        // getSet_ToD = getSettings.GetComponent<Settings>().timeOfDay;
        // getSet_TimeScale = getSettings.GetComponent<Settings>().timeScale;

        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        // transform.Rotate(new Vector3 (10*deltaTime, 0, 0));
        // _timeOfDay += deltaTime*getSet_TimeScale;
        // Debug.Log(_timeOfDay);
        // _timeOfDay += Time.deltaTime * _timeScale / 86400; // seconds in a day


        // _timeOfDay = getSet_ToD; // currently this sets the time of day but from scale of 0-24
        if(_timeOfDay > 24.0f) //new day!!
        {
            _timeOfDay = 0;
        }

		float msec = deltaTime * 1000.0f;
		float fps = 1.0f / deltaTime;

        float currentTime = timeServer.GetTime();
        TimeSpan timeSpan = TimeSpan.FromSeconds(currentTime);

         _timeOfDay = (currentTime/86400.0f);
         Debug.Log(_timeOfDay);
         // as set on line 125 we just add the simulation time seconds to it again and again so it becomes 7+x, 7+y where y>x

        // _timeOfDay = _timeOfDay + (sim_time_float/3200.0f);

        // elapsedTime += Time.deltaTime;
        elapsedTime = currentTime;

    }

    //rotates the sun daily (and seasonally soon too);
    private void AdjustSunRotation()
    {
        float sunAngle = (_timeOfDay * 360f)/24.0f;
        // Debug.Log(sunAngle);
        dailyRotation.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, sunAngle));

        // float seasonalAngle = -maxSeasonalTilt * Mathf.Cos(dayNumber / yearLength * 2f * Mathf.PI);
        // sunSeasonalRotation.localRotation = Quaternion.Euler(new Vector3(seasonalAngle, 0f, 0f));
    }

    private void SunIntensity()
    {
        intensity = Vector3.Dot(sun.transform.forward, Vector3.down);
        intensity = Mathf.Clamp01(intensity);

        sun.intensity = intensity * sunVariation + sunBaseIntensity;
    }

    private void AdjustSunColor()
    {
        sun.color = sunColor.Evaluate(intensity);
    }

    public void AddModule(DNModuleBase module)
    {
        moduleList.Add(module);
    }

    public void RemoveModule(DNModuleBase module)
    {
        moduleList.Remove(module);
    }

    //update each module based on current sun intensity
    private void UpdateModules()
    {
        foreach (DNModuleBase module in moduleList)
        {
            module.UpdateModule(intensity);
        }
    }

    public void EnableDisableSunMoon(bool enable)
    {
        int allLayersMask = ~0; // ~0 is for `Everything` and 0 is for `Nothing`
        sun.cullingMask = enable ? allLayersMask : 0; 
        foreach (DNModuleBase module in moduleList)
        {
            int new_mask = enable ? allLayersMask : 0;
            module.CullingMask(new_mask);
        }
    }

}
